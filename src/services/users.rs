use crate::db::models::*;
use crate::db::schema::users::dsl::{email, users};
use crate::db::PgPool;
use crate::handlers::auth::{LoginUserInput, RegisterUserInput};
use anyhow::Result;
use data_encoding::HEXUPPER;
use diesel::prelude::*;
use ring::rand::SecureRandom;
use ring::{digest, pbkdf2, rand};
use std::num::NonZeroU32;

const CREDENTIAL_LEN: usize = digest::SHA512_OUTPUT_LEN;

lazy_static! {
    static ref N_ITER: NonZeroU32 = NonZeroU32::new(100_000).unwrap();
}

pub struct UserService {
    db: PgPool,
}

impl UserService {
    pub fn new(db: PgPool) -> Self {
        Self { db }
    }

    pub fn get_users(&self) -> Result<Vec<User>> {
        let connection = self.db.get()?;
        let results = users.load::<User>(&connection)?;
        Ok(results)
    }

    pub fn register_user(&self, user: &RegisterUserInput) -> Result<User> {
        let connection = self.db.get()?;
        match users
            .filter(email.eq(&user.email))
            .first::<User>(&connection)
        {
            Ok(user) => Err(anyhow!("email {} is already used", user.email)),
            Err(_) => {
                let (salt, password_hash) = hash_password(user.password.clone())?;
                let new_user = NewUser {
                    username: &user.username,
                    email: &user.email,
                    salt: &salt,
                    password_hash: &password_hash,
                };
                diesel::insert_into(users)
                    .values(&new_user)
                    .execute(&connection)?;
                let user = users
                    .filter(email.eq(&user.email))
                    .first::<User>(&connection)?;
                Ok(user)
            }
        }
    }

    pub fn login_user(&self, input: &LoginUserInput) -> Result<User> {
        let connection = self.db.get()?;
        let user = users
            .filter(email.eq(&input.email))
            .first::<User>(&connection)
            .map_err(|e| anyhow!("{}", e));
        user.and_then(|user| {
            verify_password(
                input.password.clone(),
                user.salt.clone(),
                user.password_hash.clone(),
            )?;
            Ok(user)
        })
        .map_err(|_| anyhow!("invalid user or password"))
    }
}

fn hash_password(password: String) -> Result<(String, String)> {
    let rng = rand::SystemRandom::new();
    // Generate salt
    let mut salt = [0u8; CREDENTIAL_LEN];
    rng.fill(&mut salt)
        .map_err(|e| anyhow!("unable to generate salt: {}", e))?;
    let mut pbkdf2_hash = [0u8; CREDENTIAL_LEN];
    pbkdf2::derive(
        pbkdf2::PBKDF2_HMAC_SHA512,
        *N_ITER,
        &salt,
        password.as_bytes(),
        &mut pbkdf2_hash,
    );
    Ok((HEXUPPER.encode(&salt), HEXUPPER.encode(&pbkdf2_hash)))
}

fn verify_password(password: String, salt: String, password_hash: String) -> Result<()> {
    let salt = HEXUPPER.decode(salt.as_bytes())?;
    let pbkdf2_hash = HEXUPPER.decode(password_hash.as_bytes())?;
    pbkdf2::verify(
        pbkdf2::PBKDF2_HMAC_SHA512,
        *N_ITER,
        &salt,
        password.as_bytes(),
        &pbkdf2_hash,
    )
    .map_err(|_| anyhow!("invalid password"))
}
