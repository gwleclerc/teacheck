use actix_web::{error::ResponseError, HttpResponse};
use failure::Fail;
use serde::Serialize;

#[derive(Serialize)]
pub struct Error {
    pub message: String,
}

#[derive(Fail, Debug)]
pub enum TeaCheckError {
    #[fail(display = "Internal Server Error: {}", _0)]
    InternalServerError(String),

    #[fail(display = "BadRequest: {}", _0)]
    BadRequest(String),

    #[fail(display = "Unauthorized")]
    Unauthorized,

    #[fail(display = "Not Found")]
    NotFound,
}

impl ResponseError for TeaCheckError {
    fn error_response(&self) -> HttpResponse {
        match &*self {
            TeaCheckError::InternalServerError(_) => {
                HttpResponse::InternalServerError().json(Error {
                    message: String::from("Internal Server Error, Please try later"),
                })
            }
            TeaCheckError::BadRequest(message) => HttpResponse::BadRequest().json(Error {
                message: message.clone(),
            }),
            TeaCheckError::Unauthorized => HttpResponse::Unauthorized().json(Error {
                message: String::from("Unauthorized"),
            }),
            TeaCheckError::NotFound => HttpResponse::NotFound().json(Error {
                message: String::from("Not Found"),
            }),
        }
    }
}

impl From<anyhow::Error> for TeaCheckError {
    fn from(err: anyhow::Error) -> TeaCheckError {
        TeaCheckError::InternalServerError(err.to_string())
    }
}
