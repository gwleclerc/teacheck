use crate::errors::TeaCheckError;
use crate::services::Services;
use actix_web::{web, HttpResponse, Responder};
use serde::Deserialize;

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("auth")
            .route("/register", web::post().to(register))
            .route("/login", web::post().to(login)),
    );
}

#[derive(Debug, Deserialize)]
pub struct RegisterUserInput {
    pub username: String,
    pub email: String,
    pub password: String,
}

async fn register(data: web::Data<Services>, item: web::Json<RegisterUserInput>) -> impl Responder {
    let input = item.into_inner();
    match data.users.register_user(&input) {
        Ok(user) => Ok(HttpResponse::Ok().json(user)),
        Err(err) => Err(TeaCheckError::BadRequest(err.to_string())),
    }
}

#[derive(Debug, Deserialize)]
pub struct LoginUserInput {
    pub email: String,
    pub password: String,
}

async fn login(data: web::Data<Services>, item: web::Json<LoginUserInput>) -> impl Responder {
    let input = item.into_inner();
    match data.users.login_user(&input) {
        Ok(user) => Ok(HttpResponse::Ok().json(user)),
        Err(err) => Err(TeaCheckError::BadRequest(err.to_string())),
    }
}
