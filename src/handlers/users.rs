use crate::errors::TeaCheckError;
use crate::services::Services;
use actix_web::{web, HttpResponse, Responder};

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(web::scope("/users").route("", web::get().to(gets_users)));
}

async fn gets_users(data: web::Data<Services>) -> impl Responder {
    match data.users.get_users() {
        Ok(users) => Ok(HttpResponse::Ok().json(users)),
        Err(err) => Err(TeaCheckError::from(err)),
    }
}
