use crate::errors::TeaCheckError;
use actix_web::{error::ResponseError, HttpResponse};

pub mod auth;
pub mod users;

pub async fn not_found() -> HttpResponse {
    TeaCheckError::NotFound.error_response()
}
