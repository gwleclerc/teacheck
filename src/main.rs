#![warn(clippy::all)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate anyhow;
#[macro_use]
extern crate lazy_static;
extern crate chrono;

mod db;
mod errors;
mod handlers;
mod services;

use anyhow::Result;
use std::env;

#[actix_rt::main]
async fn main() -> Result<()> {
    use actix_web::{middleware, web, App, HttpServer};
    env::set_var("RUST_LOG", "info");
    env_logger::init();

    let pool = db::establish_connection_pool()?;

    HttpServer::new(move || {
        let users = services::UserService::new(pool.clone());
        App::new()
            .wrap(middleware::Logger::default())
            .wrap(middleware::Compress::default())
            .data(services::Services { users })
            .configure(handlers::auth::config)
            .configure(handlers::users::config)
            .default_service(web::route().to(handlers::not_found))
    })
    .bind("127.0.0.1:8088")?
    .run()
    .await?;
    Ok(())
}
