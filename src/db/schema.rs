table! {
    users (id) {
        id -> Integer,
        username -> Text,
        email -> Text,
        salt -> Text,
        password_hash -> Text,
        created_at -> Timestamp,
    }
}
