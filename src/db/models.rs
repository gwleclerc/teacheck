use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};

use crate::db::schema::*;

#[derive(Queryable, Serialize, Deserialize, Debug)]
pub struct User {
    pub id: i32,
    pub username: String,
    pub email: String,

    #[serde(skip_serializing)]
    pub salt: String,
    #[serde(skip_serializing)]
    pub password_hash: String,

    pub created_at: NaiveDateTime,
}
#[derive(Insertable, Debug)]
#[table_name = "users"]
pub struct NewUser<'a> {
    pub username: &'a str,
    pub email: &'a str,
    pub salt: &'a str,
    pub password_hash: &'a str,
}
