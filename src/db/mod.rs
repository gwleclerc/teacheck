use diesel::sqlite::SqliteConnection;
use dotenv::dotenv;
use std::env;

use anyhow::Result;
use diesel::r2d2::{ConnectionManager, Pool, PoolError};

pub type PgPool = Pool<ConnectionManager<SqliteConnection>>;

pub mod models;
pub mod schema;

fn init_pool(database_url: &str) -> Result<PgPool, PoolError> {
    let manager = ConnectionManager::<SqliteConnection>::new(database_url);
    Pool::builder().build(manager)
}

pub fn establish_connection_pool() -> Result<PgPool> {
    dotenv().ok();

    let database_url = env::var("DATABASE_URL")?;
    Ok(init_pool(&database_url)?)
}
