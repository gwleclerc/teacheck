**TeaCheck** (team check) is a shared tool which aims to provide virtual support to the Spotify's [Squad Health Check Model](https://engineering.atspotify.com/2014/09/16/squad-health-check-model/).

## Table of contents

- [Installation](#installation)
- [Usage](#usage)
- [Development](#development)
  - [Backend](#backend)
- [Authors](#authors)
- [Contributors](#contributors)

## Installation

TODO

## Usage

TODO

## Development

### Backend

The backend is written in Rust. You can use the following commands to manage the development lifecycle:

- `make start`: start the backend in development mode
- `make build`: compile the code
- `make lint`: run static analysis on the code
- `make test`: execute unit tests
- `make reset-db`: init sqlite databse + perform SQL migrations

In order to start properly this application, you nedd:

- SQLite installed on your desk

  ```
  # on OpenSUSE
  sudo zypper install sqlite3-devel libsqlite3-0 sqlite3

  # on Ubuntu
  sudo apt-get install libsqlite3-dev sqlite3

  # on Fedora
  sudo dnf install libsqlite3x-devel sqlite3x

  # on macOS (using homebrew)
  brew install sqlite3
  ```

- Define a database to use

  ```
  echo "DATABASE_URL=test.db" > .env
  make reset-db
  ```

## Authors

- Gwendal Leclerc: [Gitlab Profile](https://gitlab.com/gwleclerc), [Github Profile](https://github.com/gwleclerc)
