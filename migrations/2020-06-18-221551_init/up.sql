-- Your SQL goes here

CREATE TABLE users (
  id INTEGER PRIMARY KEY NOT NULL,
  username VARCHAR NOT NULL,
  email VARCHAR NOT NULL,
  salt VARCHAR NOT NULL,
  password_hash VARCHAR NOT NULL,
  created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL
);
CREATE UNIQUE INDEX idx_users_email 
ON users (email);

INSERT INTO users (username, email, salt, password_hash)
VALUES
  ('toto', 'toto@toto.toto', 'salt1', 'hash1'),
  ('tata', 'tata@tata.tata', 'salt3', 'hash3');
