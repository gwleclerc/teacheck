.PHONY: default
default: start

WATCH=$(HOME)/.cargo/bin/cargo-watch
$(WATCH):
	cargo install cargo-watch

DIESEL=$(HOME)/.cargo/bin/diesel
$(DIESEL):
	cargo install diesel_cli --no-default-features --features sqlite

.PHONY: start
start: $(WATCH)
	cargo watch -w src -x run

.PHONY: build
build:
	cargo build --release

.PHONY: lint
lint:
	cargo clippy

.PHONY: test
test:
	cargo test

.PHONY: reset-db
reset-db: $(DIESEL)
	diesel migration redo
